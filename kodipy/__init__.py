# -*- coding: utf-8 -*-

# Kodipy - simple module to remotly access Kodi
# https://www.florian-diesch.de/software/kodipy/
#
# Copyright (C) 2019 Florian Diesch devel@florian-diesch.de
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json
import requests
import functools

@functools.total_ordering
class Data:

    def __init__(self, data):
        self._raw_data = data

    def _get_item(self, item):
        if isinstance(item, (dict, list, tuple)):
            return Data(item)
        else:
            return item

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return self._raw_data == other._raw_data
        else:
            return self._raw_data == other

    def __lt__(self, other):
        if isinstance(other, type(self)):
            return self._raw_data < other._raw_data
        else:
            return self._raw_data < other
        
    def __repr__(self):
        return repr(self._raw_data)
    
    def __str__(self):
        return str(self._raw_data)

    def __iter__(self):
        if isinstance(self._raw_data, dict):
            yield from self._raw_data.items()
        else:
            yield from self._raw_data

    def __getitem__(self, index):
        return self._get_item(self._raw_data[index])
        
    def __getattr__(self, name):
        try:
            item = getattr(self._raw_data, name)
        except AttributeError:
            item = self._raw_data[name]
        return self._get_item(item)

    def __bool__(self):
        return bool(self._raw_data)

    def __len__(self):
        return len(self._raw_data)

    def __contains__(self, item):
        return item in self._raw_data
        
class APIException(Exception):

    def __init__(self, method, data):
        msg = 'API error {} while calling "{}": {}'.format(
            data['error']['code'],
            method,
            data['error']['message'])
        Exception.__init__(self, msg)
        self._method = method
        self._data = data

    def __getattr__(self, name):
        return self._data[name]

    
class Namespace:

    def __init__(self, api, name):
        self.api = api
        self.name = name

    def __getattr__(self, name):
        def func(**params):
            method = '{}.{}'.format(self.name, name)
            result = self.api.call(method=method, params=params)
            if 'error' in result:
                raise APIException(method, result)
            else:
                return Data(result['result'])
        return func
        

class Api:

    def __init__(self, url, user='kodi', password='kodi'):
        self.url = url
        self.user = user
        self.password = password
        self._id = 0

    def _get(self, method, params):
          data = json.dumps({'jsonrpc': '2.0',
                 'id': self.id,
                 'method': method,
                 'params': params
            })
          return requests.post(self.url,
                     data,
                     headers={'Content-Type': 'application/json'},
                     auth=(self.user, self.password)
          ).json()
        
    def call(self, method, params):
        return self._get(method, params)
        
    @property
    def id(self):
        self._id += 1
        return self._id

    def __getattr__(self, name):
        return Namespace(api=self, name=name)

