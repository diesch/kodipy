# -*- coding: utf-8 -*-
# Kodipy - simple module to remotly access Kodi
# https://www.florian-diesch.de/software/kodipy/
#
# Copyright (C) 2019 Florian Diesch devel@florian-diesch.de
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

NAME='kodipy'
TITLE='Kodipy'
VERSION='0.1'
AUTHOR_NAME='Florian Diesch'
AUTHOR_EMAIL='devel@florian-diesch.de'
DESC='simple module to remotly access Kodi'
WEB_URL='https://www.florian-diesch.de/software/kodipy/'
TIMESTAMP='2019-02-02T20:56:01'
PPA='diesch/testing'
DISTRIBUTION='cosmic'
GPG_KEY='B57F5641'
