#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest
import kodipy
import requests
from unittest.mock import MagicMock, patch



def test_init():
    api = kodipy.Api(url='bla')

    assert api.url == 'bla'
    assert api.user == 'kodi'
    assert api.password == 'kodi'
    assert api._id == 0

def test_id():
     api = kodipy.Api(url='bla')

     assert api.id == 1
     assert api.id == 2
     assert api.id == 3

     
def test_get():
    with patch('requests.post') as req_post:
        api = kodipy.Api(url='bla')
        api.Bla.foo()
    
        assert req_post.called
        assert req_post.call_args == (
            ('bla',
             '{"jsonrpc": "2.0", "id": 1, "method": "Bla.foo", "params": {}}'
            ),
            {'auth': ('kodi', 'kodi'),
             'headers': {'Content-Type': 'application/json'}
            }
        )
    
    

