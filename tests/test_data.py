#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pytest

import kodipy


def test_dict():
    adict = {'foo': 1, 'bar': 'a'}
    data = kodipy.Data(adict)

    assert data.foo == 1
    assert data.bar == 'a'
    assert data.items
    

def test_nested_dict():
    adict = {'d': {'key1': 1, 'key2': 2}}
    data = kodipy.Data(adict)
    
    assert isinstance(data.d, kodipy.Data)
    assert data.d.key1 == 1
    assert data.d.key2 == 2


def test_list():
    adict = {'l':[1, 2, 3, 4, 5]}
    data = kodipy.Data(adict)

    assert isinstance(data.l, kodipy.Data)

    assert data.l[0] == 1
    assert data.l[:3] == [1, 2, 3]

def test_nested_list():
    
    adict = {'l':[1, 2, {'a': 99, 'b': 100}]}
    data = kodipy.Data(adict)

    assert data.l == [1, 2, {'a': 99, 'b': 100}]
    assert data.l[2].a == 99
    assert data.l[2].b == 100

def test_compare():
    alist = [1,2, [3,4]]
    data = kodipy.Data(alist)

    assert data == kodipy.Data(alist)
    assert data < kodipy.Data([2,3])
    assert data > kodipy.Data([0,3])

    assert data == [1,2, [3,4]]
    assert data >= [1,2, [3,4]]
    assert data < [2]
    assert data <= [2]
    assert not data > [2]

    assert data[2] == [3,4]
    assert data[2] <= [3,4]
    assert not data[2] > [3,4]
    assert not data[2] < [3,4]

def test_bool():
    adict = {'true': [1], 'false':[], 'emptydict':{}, 'adict':{'bla': 99}}

    data = kodipy.Data(adict)
    assert data
    assert data.true
    assert data.true[0]
    assert not data.false
    assert not data.emptydict
    assert data.adict

def test_len():
    adict = {'_1':0, '_2':{'foo': 'bar'}, '_3':[], '_4':[{}, {1:2}]}
    data = kodipy.Data(adict)

    assert len(data) == 4
    assert len(data._2) == 1
    assert len(data._3) == 0
    assert len(data._4) == 2
    assert len(data._4[0]) == 0
    assert len(data._4[1]) == 1

def test_iter():
    adict = {'a': 'A', 'b': [1,2,3,4], 'c': {'foo':1, 'bar':2}}
    data = kodipy.Data(adict)
    
    assert [k for k in data] == [('a', 'A'), ('b', [1,2,3,4]),
                                 ('c',  {'foo':1, 'bar':2})]
    assert [k for k in data.b] == [1, 2, 3, 4]
    assert [k for k in data.c] == [('foo', 1), ('bar', 2)]

def test_contains():
    adict = {'a': 'A', 'b': [1,2,3,4], 'c': {'foo':1, 'bar':2}}
    data = kodipy.Data(adict)

    assert 'a' in data
    assert 1 not in data
    assert 2 in data.b
    assert 'b' not in data.b
    assert 'bar' in data.c
    assert 2 not in data.c
    assert 'c' not in data.c
    
def test_str():
    adict = {'a': 'A', 'b': [1,2,3,4], 'c': {'foo':1, 'bar':2}}
    data = kodipy.Data(adict)

    assert str(data) == str(adict)


def test_repr():
    adict = {'_1':0, '_2':{'foo': 'bar'}, '_3':[], '_4':[{}, {1:2}]}
    data = kodipy.Data(adict)

    assert repr(data) == repr(adict)
    

    
    
    

    

